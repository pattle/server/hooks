# Copyright (C) 2018  Louis Matthijssen <louis@u5r.nl>
# Copyright (C) 2018  Wilko Manger <wilko@rens.onl>
#
# This file is part of Hooks.
#
# Hooks is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hooks is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hooks.  If not, see <https://www.gnu.org/licenses/>.

import os


class Settings:
    """Contains all configurable settings for Hooks."""

    @property
    def http_port(self):
        """Gets the HTTP port to listen on, using 80 as default.

        :return: HTTP port to listen on.
        """
        value = os.environ.get('HTTP_PORT')
        return 80 if value is None else int(value)

    @property
    def web_hook_token(self):
        """Gets the GitLab WebHook token to use.

        :return: GitLab WebHook token to use.
        """
        return os.environ.get('WEB_HOOK_TOKEN')

    @property
    def api_token(self):
        """Gets the GitLab API token to use.

        :return: GitLab API token to use.
        """
        return os.environ.get('API_TOKEN')

    @property
    def api_url(self):
        """Gets the URL of the GitLab API to use.

        :return: URL of the GitLab API to use.
        """
        return os.environ.get('API_URL')

    @property
    def excluded_owner_ids(self):
        """Gets a list of owner IDs that are excluded from the project creation restrictions.
        Note: the owner IDs should be separated by just a comma, no spaces!

        :return: List of owner IDs that are excluded from the project creation restrictions.
        """
        return os.environ.get('EXCLUDED_OWNER_IDS').split(',')

    @property
    def email_user_name(self):
        """Gets the user name used to log in on the email server.
        If no user name is set, email authentication won't be used.

        :return: User name used to log in on the email server.
        """
        return os.environ.get('EMAIL_USER_NAME')

    @property
    def email_password(self):
        """Gets the password used to log in on the email server.
        If no password is set, email authentication won't be used.

        :return: Password used to log in on the email server.
        """
        return os.environ.get('EMAIL_PASSWORD')

    @property
    def email_server(self):
        """Gets the host name or the IP address of the email server.

        :return: Host name or IP address of the email server.
        """
        return os.environ.get('EMAIL_SERVER')

    @property
    def email_from(self):
        """Gets the from address that should be used for sending emails.

        :return: From address that should be used for sending emails.
        """
        return os.environ.get('EMAIL_FROM')

    @property
    def email_bcc(self):
        """Gets the BCC address that should be used for sending emails.

        :return: BCC address that should be used for sending emails.
        """
        return os.environ.get('EMAIL_BCC')

    @property
    def email_project_deleted_subject(self):
        """Gets the subject for the 'project deleted' email.

        :return: Subject for the 'project deleted' email.
        """
        return os.environ.get('EMAIL_PROJECT_DELETED_SUBJECT')

    @property
    def email_project_deleted_body(self):
        """Gets the body for the 'project deleted' email.

        :return: Body for the 'project deleted' email.
        """
        return os.environ.get('EMAIL_PROJECT_DELETED_BODY')


settings = Settings()

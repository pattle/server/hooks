# Copyright (C) 2018  Louis Matthijssen <louis@u5r.nl>
# Copyright (C) 2018  Wilko Manger <wilko@rens.onl>
#
# This file is part of Hooks.
#
# Hooks is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hooks is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hooks.  If not, see <https://www.gnu.org/licenses/>.

import json

from http.server import BaseHTTPRequestHandler
from settings import settings


class WebHookHandler(BaseHTTPRequestHandler):
    """HTTP request handler that handles incoming GitLab WebHooks."""
    queue = None

    def do_POST(self):
        """Handles incoming POST requests.
        These requests will be validated.
        Only valid (verified) requests will be passed to the queue in order to be processed.
        """
        token = self.headers.get('X-Gitlab-Token')

        if token != settings.web_hook_token:
            self.log_error('Received WebHook request with invalid token')
            self.send_final_response(403, 'Invalid WebHook token')
            return

        event = self.headers.get('X-Gitlab-Event')

        if event != 'System Hook':
            self.log_error('Received WebHook request with invalid event {}'.format(event))
            self.send_final_response(404, 'Invalid WebHook event')
            return

        try:
            content_length = self.headers.get('Content-Length')
            body = self.rfile.read(int(content_length))
            self.queue.put(json.loads(body))

            self.log_message('Received valid WebHook request for event {}'.format(event))
            self.send_final_response(200, 'Valid WebHook request')
        except ValueError:
            self.log_error('Received malformed WebHook request for event {}'.format(event))
            self.send_final_response(400, 'Malformed WebHook request')
            return

    def send_final_response(self, code, message):
        """Sends a response and ends the request.

        :param code: HTTP status code to use.
        :param message: Message to send.
        """
        self.send_response(code)
        self.send_header('Content-Type', 'text/plain')
        self.end_headers()

        self.wfile.write(bytes(message, 'utf8'))

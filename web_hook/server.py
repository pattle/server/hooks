# Copyright (C) 2018  Louis Matthijssen <louis@u5r.nl>
# Copyright (C) 2018  Wilko Manger <wilko@rens.onl>
#
# This file is part of Hooks.
#
# Hooks is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hooks is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hooks.  If not, see <https://www.gnu.org/licenses/>.

from http.server import HTTPServer


class WebHookServer(HTTPServer):
    """HTTP server with support for the WebHookHandler."""

    def serve_web_hook_forever(self, queue):
        """Starts the HTTP server, passing the queue to the WebHookHandler.

        :param queue Queue to write incoming validated WebHook events to.
        """
        self.RequestHandlerClass.queue = queue
        HTTPServer.serve_forever(self)

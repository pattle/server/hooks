# Copyright (C) 2018  Louis Matthijssen <louis@u5r.nl>
# Copyright (C) 2018  Wilko Manger <wilko@rens.onl>
#
# This file is part of Hooks.
#
# Hooks is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hooks is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hooks.  If not, see <https://www.gnu.org/licenses/>.

import logging

from threading import Thread
from queue import Queue
from event import EventHandler
from settings import settings
from web_hook import WebHookHandler, WebHookServer


def run():
    """Initializes and starts all required components."""
    queue = Queue()
    event_handler = EventHandler(queue)

    thread = Thread(target=event_handler.run, name='Event Handler', daemon=True)
    httpd = WebHookServer(('', settings.http_port), WebHookHandler)

    thread.start()
    httpd.serve_web_hook_forever(queue)
    queue.join()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    run()

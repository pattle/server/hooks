# Copyright (C) 2018  Louis Matthijssen <louis@u5r.nl>
# Copyright (C) 2018  Wilko Manger <wilko@rens.onl>
#
# This file is part of Hooks.
#
# Hooks is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hooks is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hooks.  If not, see <https://www.gnu.org/licenses/>.

import logging

from .project_create_handler import EventProjectCreateHandler


class EventHandler:
    """Handles incoming WebHook events by dispatching them to a specific handler."""
    logger = logging.getLogger('EventHandler')
    handlers = {'project_create': EventProjectCreateHandler()}
    queue = None

    def __init__(self, queue):
        """Initializes the event handler using the given queue.

        :param queue Queue containing validated WebHook events.
        """
        self.queue = queue

    def run(self):
        """Starts consuming and dispatching incoming WebHook events."""
        while True:
            payload = self.queue.get()
            self.logger.info('Processing new WebHook payload')

            if 'event_name' not in payload:
                self.logger.error('Payload does not contain event name')
                self.queue.task_done()
                continue

            event_name = payload['event_name']

            if event_name not in self.handlers:
                self.logger.warning('No handler registered for event {}'.format(event_name))
                self.queue.task_done()
                continue

            try:
                self.handlers[event_name].handle(payload)
            except Exception:
                self.logger.exception('Exception occurred while executing event handler')

            self.queue.task_done()

# Copyright (C) 2018  Louis Matthijssen <louis@u5r.nl>
# Copyright (C) 2018  Wilko Manger <wilko@rens.onl>
#
# This file is part of Hooks.
#
# Hooks is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hooks is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hooks.  If not, see <https://www.gnu.org/licenses/>.

import json
import logging

from api.client import api_client
from message.manager import message_manager
from settings import settings


class EventProjectCreateHandler:
    """WebHook event handler that handles created projects."""
    logger = logging.getLogger('EventProjectCreateHandler')

    def handle(self, payload):
        """Handles an incoming WebHook event which checks if the created project is allowed.
        This event handler will delete projects which are not allowed.

        :param payload WebHook event data.
        """
        project_id = payload['project_id']
        project_info = api_client.get('projects/{}'.format(project_id))

        if project_info.status_code != 200:
            self.logger.warning('Failed to fetch project info for #{}'.format(project_id))
            return

        project_info_payload = json.loads(project_info.text)
        owner_id = str(project_info_payload['owner']['id'])

        if owner_id in settings.excluded_owner_ids:
            self.logger.info('Owner #{} is excluded from this check, keeping'.format(owner_id))
            return

        if 'forked_from_project' in project_info_payload:
            self.logger.info('Project #{} is a fork, keeping'.format(project_id))
            return

        self.logger.info('Project #{} is not a fork, deleting'.format(project_id))
        self.delete_project(payload)

    def delete_project(self, payload):
        """Deletes a project with the specified ID from the server.

        :param payload WebHook event data.
        """
        project_id = payload['project_id']
        project_delete = api_client.delete('projects/{}'.format(project_id))

        if project_delete.status_code != 202:
            self.logger.warning('Failed to delete project #{}'.format(project_id))
        else:
            self.logger.info('Deleted project #{}'.format(project_id))

            body = settings.email_project_deleted_body.format(payload['owner_name'], payload['name'])
            if message_manager.send_email(payload['owner_email'], settings.email_project_deleted_subject, body):
                self.logger.info('Successfully sent project deleted email for #{}'.format(project_id))
            else:
                self.logger.warning('Failed to send project deleted email for #{}'.format(project_id))

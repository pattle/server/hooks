# Hooks

Hooks is a simple tool to manage to manage a GitLab instance using WebHooks.

## Docker Compose

Hooks can be started using Docker Compose. For example:

```yaml
services:
  hooks:
    image: hooks:latest
    restart: always
    environment:
      # WebHook token to verify requests from GitLab.
      WEB_HOOK_TOKEN: '123456abcd'
      # API token to authenticate with the GitLab API.
      API_TOKEN: 'abcdef1234'
      # API URL to communicate with the GitLab API.
      API_URL: 'https://git.example/api/v4'
      # Comma separated list of owner IDs that are excluded from the check.
      EXCLUDED_OWNER_IDS: '1,2'
      # User name used to log in on the email server.
      EMAIL_USER_NAME: 'email-user'
      # Password used to log in on the email server.
      EMAIL_PASSWORD: '098765zyx'
      # Host name or IP address of the email server.
      EMAIL_SERVER: 'mail.example'
      # From address used for sending emails.
      EMAIL_FROM: 'Hooks <hooks@example>'
      # BCC address used for sending emails.
      # Don't set this to disable BCC.
      EMAIL_BCC: 'admin@example'
      # Subject for the 'project deleted' email.
      EMAIL_PROJECT_DELETED_SUBJECT: 'Project deleted'
      # Body for the 'project deleted' email.
      EMAIL_PROJECT_DELETED_BODY: |
        Dear {0},

        Your project, {1}, has been removed.
```

# Copyright (C) 2018  Louis Matthijssen <louis@u5r.nl>
# Copyright (C) 2018  Wilko Manger <wilko@rens.onl>
#
# This file is part of Hooks.
#
# Hooks is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hooks is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hooks.  If not, see <https://www.gnu.org/licenses/>.

import logging
import requests

from settings import settings


class ApiClient:
    """Provides methods for communicating with the GitLab API."""
    logger = logging.getLogger('ApiClient')
    headers = {'Private-Token': settings.api_token}

    def get(self, path, params=None):
        """Sends a GET request to the GitLab API.

        :param path Relative path to the GitLab API method.
        :param params Query string params to add.
        """
        self.logger.info('Executing GET request for {}'.format(path))
        return requests.get('{}/{}'.format(settings.api_url, path), params=params, headers=self.headers)

    def post(self, path, payload=None):
        """Sends a POST request to the GitLab API.

        :param path Relative path to the GitLab API method.
        :param payload Data to send as JSON.
        """
        self.logger.info('Executing POST request for {}'.format(path))
        return requests.post('{}/{}'.format(settings.api_url, path), json=payload, headers=self.headers)

    def delete(self, path):
        """Sends a DELETE request to the GitLab API.

        :param path Relative path to the GitLab API method.
        """
        self.logger.info('Executing DELETE request for {}'.format(path))
        return requests.delete('{}/{}'.format(settings.api_url, path), headers=self.headers)


api_client = ApiClient()

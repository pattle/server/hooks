# Copyright (C) 2018  Louis Matthijssen <louis@u5r.nl>
# Copyright (C) 2018  Wilko Manger <wilko@rens.onl>
#
# This file is part of Hooks.
#
# Hooks is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hooks is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hooks.  If not, see <https://www.gnu.org/licenses/>.

import smtplib
import logging

from email.mime.text import MIMEText
from smtplib import SMTPException
from settings import settings


class MessageManager:
    """Handles sending emails."""
    logger = logging.getLogger('MessageManager')

    def send_email(self, recipient, subject, body):
        """Sends an email to the given recipient using the provided subject and body.

        :param recipient: Email address to send the
        :param subject: Subject to use.
        :param body: Body to send.
        :return: True on success, False otherwise.
        """
        message = MIMEText(body)
        email_to = [recipient]

        if settings.email_bcc is not None:
            email_to.append(settings.email_bcc)

        message['Subject'] = subject
        message['From'] = settings.email_from
        message['To'] = recipient

        try:
            with smtplib.SMTP(settings.email_server, 587) as server:
                server.starttls()

                if settings.email_user_name is not None and settings.email_password is not None:
                    server.login(settings.email_user_name, settings.email_password)

                server.sendmail(settings.email_from, email_to, message.as_string())
                return True
        except SMTPException:
            self.logger.exception('Exception occurred while sending email')
            return False


message_manager = MessageManager()
